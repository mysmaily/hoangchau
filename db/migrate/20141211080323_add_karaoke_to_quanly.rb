class AddKaraokeToQuanly < ActiveRecord::Migration
  def change
    add_column :quanly, :karaoke, :boolean, default: false
  end
end
