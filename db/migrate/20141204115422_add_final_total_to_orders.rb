class AddFinalTotalToOrders < ActiveRecord::Migration
  def change
    add_column :orders, :final_total, :integer
  end
end
