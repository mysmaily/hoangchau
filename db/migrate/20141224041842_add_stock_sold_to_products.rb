class AddStockSoldToProducts < ActiveRecord::Migration
  def change
    add_column :products, :stock_sold, :float, default: 0
  end
end
