class AddNotedToOrders < ActiveRecord::Migration
  def change
    add_column :orders, :noted, :string
  end
end
