class AddStatusToQuanly < ActiveRecord::Migration
  def change
    add_column :quanly, :status, :string, :default => 'closed'
  end
end
