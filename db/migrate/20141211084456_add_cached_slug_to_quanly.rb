class AddCachedSlugToQuanly < ActiveRecord::Migration
  def change
    add_column :quanly, :cached_slug, :string
  end
end
