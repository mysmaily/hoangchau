class CreateOrders < ActiveRecord::Migration
  def change
    create_table :orders do |t|
      t.string :customer_name
      t.integer :customer_number
      t.integer :tax, default: 0
      t.integer :sale_off, default: 0
      t.integer :total_price, default: 0

      t.timestamps
    end
  end
end
