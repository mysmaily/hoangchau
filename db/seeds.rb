#encoding: utf-8
Quanly.delete_all
User.delete_all
Category.delete_all
Order.delete_all
Product.delete_all
table = [ {:name=>"Bàn 1"},
        {:name=>"Bàn 2"},
        {:name=>"Bàn 3"},
        {:name=>"Bàn 4"},
        {:name=>"Bàn 5"},
        {:name=>"Bàn 6"},
        {:name=>"Bàn 7"},
        {:name=>"Bàn 8"},
        {:name=>"Bàn 9"},
        {:name=>"Bàn 10"},
        {:name=>"Bàn 11"},
        {:name=>"Bàn 12"},
        {:name=>"Bàn 13"},
        {:name=>"Bàn 14"},
        {:name=>"Bàn 15"},
        {:name=>"Bàn 16"},
        {:name=>"Bàn 17"},
        {:name=>"Bàn 18"},
        {:name=>"Bàn 19"},
        {:name=>"Bàn 20"}
      ]
room = [ {:name=>"Phòng 1", :karaoke => true, :vip => false},
        {:name=>"Phòng 2", :karaoke => true, :vip => false},
        {:name=>"Phòng 3", :karaoke => true, :vip => false},
        {:name=>"Phòng 4", :karaoke => true, :vip => false},
        {:name=>"Phòng 5 (VIP)", :karaoke => true, :vip => true}
       ]
product = [ {:name=>"Lavi", :unit => "Chai", :price => 10000, category: 2},
      			{:name=>"Hoa Quả", :unit => "Đĩa", :price => 100000, category: 1},
            {:name=>"Bia Huda", :unit => "Lon", :price => 10000, category: 1},
            {:name=>"Bim bim", :unit => "Gói", :price => 5000, category: 1},
            {:name=>"Mít sấy", :unit => "Gói", :price => 30000, category: 1},
      			{:name=>"Bia Sài Gòn", :unit => "Chai", :price => 10000, category: 2},
      			{:name=>"Bia Heiniken", :unit => "Lon", :price => 10000, category: 2},
      			{:name=>"Bia Chimay", :unit => "Chai", :price => 10000, category: 2},
						{:name=>"Rượu vodka men", :unit => "Chai", :price => 120000, category: 2},
						{:name=>"Rượu vodka Nga", :unit => "Chai", :price => 300000, category: 2},
						{:name=>"Rượu Chivas 12", :unit => "Chai", :price => 500000, category: 2},
            {:name=>"Cơm trắng", :unit => "Bát", :price => 30000, category: 1},
      			{:name=>"Canh cua", :unit => "Bát", :price => 30000, category: 1},
      			{:name=>"Rau muống xào tỏi", :unit => "Đĩa", :price => 30000, category: 1},
      			{:name=>"Chân giò", :unit => "Đĩa", :price => 30000, category: 1},
      			{:name=>"Lợn mán", :unit => "Đĩa", :price => 80000, category: 1},
      			{:name=>"Vịt quay", :unit => "Con", :price => 170000, category: 1},
      			{:name=>"Gà luộc", :unit => "Con", :price => 200000, category: 1},
            {:name=>"Canh cua mùng tơi", :unit => "Bát", :price => 30000, category: 1},
            {:name=>"Thịt bò xào", :unit => "Đĩa", :price => 120000, category: 1},
            {:name=>"Ba chỉ rang cháy cạnh", :unit => "Đĩa", :price => 80000, category: 1},
            {:name=>"Bò xào thiên lý", :unit => "Đĩa", :price => 100000, category: 1},
            {:name=>"Cá rán", :unit => "Con", :price => 50000, category: 1},
            {:name=>"Cơm rang", :unit => "Đĩa", :price => 60000, category: 1},
            {:name=>"Bánh cuốn nóng", :unit => "Đĩa", :price => 50000, category: 1},
            {:name=>"Chim bồ câu", :unit => "Con", :price => 60000, category: 1},
            {:name=>"Lạc luộc", :unit => "Bát", :price => 10000, category: 1},
            {:name=>"Cocacola", :unit => "Lon", :price => 10000, category: 2},
            {:name=>"Halida", :unit => "Chai", :price => 10000, category: 2},
            {:name=>"Nước cam", :unit => "Lon", :price => 10000, category: 2},
            {:name=>"Nước Chanh", :unit => "Cốc", :price => 8000, category: 2},
            {:name=>"Gà rán", :unit => "Con", :price => 240000, category: 1},
            {:name=>"Lẩu đuôi bò", :unit => "Nồi", :price => 360000, category: 1},
            {:name=>"Lẩu đuôi lợn", :unit => "Nồi", :price => 260000, category: 1},
            {:name=>"Bánh đa", :unit => "Cái", :price => 6000, category: 1},
      			{:name=>"Phí phòng Karaoke", :unit => "Giờ", :price => 120000, category: 3},
            {:name=>"Phí phòng Karaoke VIP", :unit => "Giờ", :price => 180000, category: 3},
      			{:name=>"Phí rượu mang vào", :unit => "Chai", :price => 10000, category: 3},
						{:name=>"Thuốc lá", :unit => "Chai", :price => 10000, category: 3},
            {:name=>"Mắm tôm", :unit => "Bát", :price => 3000, category: 1}
          ]
table.each do |a|
  Quanly.create(name: a[:name])
end

Category.create(:name => "Đồ ăn")
Category.create(:name => "Đồ uống")
Category.create(:name => "Dịch vụ")
Category.create(:name => "Khác")

room.each do |a|
  Quanly.create(name: a[:name], karaoke: a[:karaoke], vip: a[:vip])
end

product.each do |a|
  Product.create(name: a[:name], unit: a[:unit], price: a[:price], category_id: a[:category])
end

User.create(email: "phuc@gmail.com", password: "hoangchau123", password_confirmation: "hoangchau123", name: "Lê Thạc Phúc", role: "admin")
User.create(email: "tuyen@gmail.com", password: "hoangchau123", password_confirmation: "hoangchau123", name: "Thanh Tuyền", role: "admin")
User.create(email: "admin@gmail.com", password: "hoangchau123", password_confirmation: "hoangchau123", name: "Hoang Nguyen", role: "manage")