Phuc::Application.routes.draw do
  get "products/index"

  get "reports/index"

  devise_for :users

  root to: "home#index"
  resources :quanly
  resources :orders
  resources :order_items
  resources :reports
  resources :products
  match '/hoa-don.php', to: 'reports#report_bill', as: 'report_bill'
  match '/update_table_status', to: 'tables#update_table_status', as: 'update_table_status'
  match '/bao-cao-doanh-thu.php', to: 'reports#report_revenue', as: 'report_revenue'
  match '/hoa-don-chi-tiet.php', to: 'reports#reports_detail', as: 'report_detail'
  match '/bao-cao-doanh-thu-theo-san-pham.php', to: 'reports#report_product', as: 'report_product'
  match '/xoa-order', to: 'orders#delete_order', as: 'delete_order'

  # The priority is based upon order of creation:
  # first created -> highest priority.

  # Sample of regular route:
  #   match 'products/:id' => 'catalog#view'
  # Keep in mind you can assign values other than :controller and :action

  # Sample of named route:
  #   match 'products/:id/purchase' => 'catalog#purchase', :as => :purchase
  # This route can be invoked with purchase_url(:id => product.id)

  # Sample resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Sample resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Sample resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Sample resource route with more complex sub-resources
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', :on => :collection
  #     end
  #   end

  # Sample resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end

  # You can have the root of your site routed with "root"
  # just remember to delete public/index.html.
  # root :to => 'welcome#index'

  # See how all your routes lay out with "rake routes"

  # This is a legacy wild controller route that's not recommended for RESTful applications.
  # Note: This route will make all actions in every controller accessible via GET requests.
  # match ':controller(/:action(/:id))(.:format)'
end
