ns4 = document.layers;
op5 = (navigator.userAgent.indexOf("Opera 5")!=-1) 
	||(navigator.userAgent.indexOf("Opera/5")!=-1);
op6 = (navigator.userAgent.indexOf("Opera 6")!=-1) 
	||(navigator.userAgent.indexOf("Opera/6")!=-1);
agt=navigator.userAgent.toLowerCase();
mac = (agt.indexOf("mac")!=-1);
ie = (agt.indexOf("msie") != -1); 
mac_ie = mac && ie;
//Them tu he thong cu Duc
function addEvent( obj, type, fn ) { 
  if ( obj.attachEvent ) { 
    obj['e'+type+fn] = fn; 
    obj[type+fn] = function(){obj['e'+type+fn]( window.event );} 
    obj.attachEvent( 'on'+type, obj[type+fn] ); 
  } else 
    obj.addEventListener( type, fn, false ); 
}
function getElemValue(name)
{
	if(typeof($(name))!='undefined')
	{
		if(typeof($(name).value)!='undefined')
		{
			return $(name).value;
		}
		if(typeof($(name).innerHTML)!='undefined')
		{
			return $(name).innerHTML;
		}
	}
}
function format_number(pnumber,decimals){
	if (isNaN(pnumber)) { return 0};
	if (pnumber=='') { return 0};
	
	var snum = new String(pnumber);	
	var sec = snum.split('.');
	var whole = parseFloat(sec[0]);
	var result = '';
	
	if(sec.length > 1){
		var dec = new String(sec[1]);		
		dec = String(parseFloat(sec[1])/Math.pow(10,(dec.length - decimals)));
		
		dec = String(whole + Math.round(parseFloat(dec))/Math.pow(10,decimals));
		
		var dot = dec.indexOf('.');
		if(dot == -1){
			dec += '.'; 
			dot = dec.indexOf('.');
		}
		while(dec.length <= dot + decimals) { dec += '0'; }
		result = dec;
	} else{
		var dot;
		var dec = new String(whole);
		dec += '.';
		dot = dec.indexOf('.');		
		while(dec.length <= dot + decimals) { dec += '0'; }
		result = dec;
	}	
	return result;
}
function roundNumber(num, dec) {
	var result = Math.round(num*Math.pow(10,dec))/Math.pow(10,dec);
	return result;
}
function vndRound($number,$precision){
	$number = parseInt($number,0);
	$precision = $precision.toString();
	$pre_len = $precision.length;
	$number = $number.toString();
	$len = $number.length;
	$result = 0;
	if(parseInt($number)<=parseInt($precision)){
		return 0;
	}else{
		$pre_len = parseInt($pre_len);
		$new_number = $number.substr(0,$len - $pre_len);
		$tail = $number.substr($number.length - $pre_len,$number.length-1);
		$tail_pad = str_pad(1,$pre_len+1,0,'STR_PAD_RIGHT');
		$tail_pad = parseInt($tail_pad);
		if(parseInt($tail) >= parseInt($precision)){
			$new_number = parseInt($new_number) + 1;	
		}
		$rerult = $new_number*$tail_pad;
		return $rerult;
	}
}
function getElementHeight(Elem) {
	if (ns4) {
		var elem = getObjNN4(document, Elem);
		return elem.clip.height;
	} else {
		if(document.getElementById) {
			var elem = document.getElementById(Elem);
		} else if (document.all){
			var elem = document.all[Elem];
		}
		if (op5) { 
			xPos = elem.style.pixelHeight;
		} else {
			xPos = elem.offsetHeight;
		}
		return xPos;
	} 
}
function clickage(module_name,ids,arr_name,color_change)
{
	eval('array='+arr_name);
	if(!event.shiftKey)
	{
		oldInput=ids;
		$(module_name+'_check_'+ids).checked=$(module_name+'_check_'+ids).checked?1:0;
		$(module_name+'_tr_'+array[ids]).style.backgroundColor=$(module_name+'_check_'+ids).checked?color_change:'white';
		return false;
	}
	$(module_name+'_check_'+ids).checked=1;
	var low=Math.min(ids,oldInput);
	var high=Math.max(ids,oldInput)
	var uncheck=1;
	for(var i=low;i<=high;i++)
	{
		uncheck &= $(module_name+'_check_'+i).checked;
		$(module_name+'_check_'+i).checked=1;
	}
	if(uncheck)
	{
		for(i=low;i<=high;i++)
		{
			$(module_name+'_check_'+i).checked=0;
		}
	}
	change_bg(module_name,array,color_change);
	return true;
}
//
function echo(st)
{
	document.write(st);
}
function $(id)
{
	if(typeof(id)=='object')
	{
		return id;
	}
	return document.getElementById(id);
}
function toggle(id, status)
{
	if($(id))
	{
		if(typeof(status)!='undefined')
		{
			$(id).style.display = status;
		}
		else if($(id).style.display == 'none')
		{
			$(id).style.display ='';
		}
		else
		{
			$(id).style.display = 'none';
		}
	}
}
function findPos(obj) {
	var curleft = curtop = 0;
	if (obj.offsetParent) {
		do {
			curleft += obj.offsetLeft;
			curtop += obj.offsetTop;
		} while (obj = obj.offsetParent);
	}
	return [curleft,curtop];
}

function select_all_checkbox(form,name,status, select_color, unselect_color)
{
	for (var i = 0; i < form.elements.length; i++) {
		if (form.elements[i].name == 'selected_ids[]') {
			if(status==-1)
			{
				form.elements[i].checked = !form.elements[i].checked;
			}
			else
			{
				form.elements[i].checked = status;
			}
			if(select_color)
			{
				if($(name+'_tr_'+form.elements[i].value))
				{
					$(name+'_tr_'+form.elements[i].value).style.backgroundColor=
						form.elements[i].checked?select_color:unselect_color;
				}
			}
		}
	}
}
function select_checkbox(form, name, checkbox, select_color, unselect_color)
{
	tr_color = checkbox.checked?select_color:unselect_color;
	if(typeof(event)=='undefined' || !event.shiftKey)
	{
		$(name+'_all_checkbox').lastSelected = checkbox;
		if(select_color && $(name+'_tr_'+checkbox.value))
		{
			$(name+'_tr_'+checkbox.value).style.backgroundColor=
				checkbox.checked?select_color:unselect_color;
		}
		update_all_checkbox_status(form, name);
		return;
	}
	//select_all_checkbox(form, name, false, select_color, unselect_color);
	
	var active = typeof($(name+'_all_checkbox').lastSelected)=='undefined'?true:false;
	
	for (var i = 0; i < form.elements.length; i++) {
		if (!active && form.elements[i]==$(name+'_all_checkbox').lastSelected)
		{
			active = 1;
		}
		if (!active && form.elements[i]==checkbox)
		{
			active = 2;
		}
		if (active && form.elements[i].id == name+'_checkbox') {
			form.elements[i].checked = checkbox.checked;
			$(name+'_tr_'+form.elements[i].value).style.backgroundColor=
				checkbox.checked?select_color:unselect_color;
		}
		if(active && (form.elements[i]==checkbox && active==1) || (form.elements[i]==$(name+'_all_checkbox').lastSelected && active==2))
		{
			break;
		}
	}
	update_all_checkbox_status(form, name);
}
function update_all_checkbox_status(form, name)
{
	var status = true;
	for (var i = 0; i < form.elements.length; i++) {
		if (form.elements[i].name == 'selected_ids[]' && !form.elements[i].checked) {
			status = false;
			break;
		}
	}
	$(name+'_all_checkbox').checked = status;
}
/*---------------------Ham check_all,select_invert cua he thong cu ngocnv updated 11/05/09-----------------------------------*/
function check_all(module_name,array,color_change,value)
{
	//eval('array='+array);
	var inputs = document.getElementsByTagName('INPUT');
	for (var i=0;i<inputs.length;i++)
	{
		if(typeof(inputs[i])!='undefined' && inputs[i].type=='checkbox' && inputs[i].id.indexOf(module_name+'_check_')==0)
		{
			var index=inputs[i].id.substr((module_name+'_check_').length);
			if(value)
			{
				
				$(module_name+'_check_'+index).checked=1;
				if($(module_name+'_tr_'+array[index]))$(module_name+'_tr_'+array[index]).style.backgroundColor=color_change;
			}
			else
			{
				$(module_name+'_check_'+index).checked=0;	
				if($(module_name+'_tr_'+array[index]))$(module_name+'_tr_'+array[index]).style.backgroundColor='white';
			}
		}
	}
	$(module_name+'_check_0').checked=$(module_name+'_check_1').checked;	
}
function select_invert(module_name,array,color_change)
{
	eval('array='+array);
	for (i=1;i<=array['length'];i++)
	{
		$(module_name+'_check_'+i).checked=$(module_name+'_check_'+i).checked?0:1;	
		$(module_name+'_tr_'+array[i]).style.backgroundColor=$(module_name+'_check_'+i).checked?color_change:'white';
	}
}

/*-----------------------------------------------------------------------------------*/
var ns = (navigator.appName.indexOf("Netscape") != -1);
var d = document;
var px = document.layers ? "" : "px";
function JSFX_FloatDiv(id, sx, sy)
{
	var el=d.getElementById?d.getElementById(id):d.all?d.all[id]:d.layers[id];
	window[id + "_obj"] = el;
	if(d.layers)el.style=el;
	el.cx = el.sx = sx;el.cy = el.sy = sy;
	el.sP=function(x,y){this.style.left=x+px;this.style.top=y+px;};
	el.flt=function()
	{
		var pX, pY;
		pX = (this.sx >= 0) ? 0 : ns ? innerWidth : 
		document.documentElement && document.documentElement.clientWidth ? 
		document.documentElement.clientWidth : document.body.clientWidth;
		pY = ns ? pageYOffset : document.documentElement && document.documentElement.scrollTop ? 
		document.documentElement.scrollTop : document.body.scrollTop;
		if(this.sy<0) 
		pY += ns ? innerHeight : document.documentElement && document.documentElement.clientHeight ? 
		document.documentElement.clientHeight : document.body.clientHeight;
		this.cx += (pX + this.sx - this.cx)/8;this.cy += (pY + this.sy - this.cy)/8;
		this.sP(this.cx, this.cy);
		setTimeout(this.id + "_obj.flt()", 40);
	}
	return el;
}
function number_format(nStr)
{
	nStr += '';
	x = nStr.split('.');
	x1 = x[0];
	x2 = x.length > 1 ? '.' + x[1] : '';
	var rgx = /(\d+)(\d{3})/;
	while (rgx.test(x1)) {
		x1 = x1.replace(rgx, '$1' + ',' + '$2');
	}
	return x1 + x2;
	
}
/*-------------------- Edit by ngocnv add function to_numeric--------------*/
function to_numeric(st)
{
	st = st+'';
	if(st)
	{
		return (typeof(st)=='number' || (typeof(st.match)!='undefined' && !st.match(/[^0-9.,-]/)))?parseFloat(st.replace(/\,/g,'')):st;
	}
	else
	{
		return st;
	}
}
/*-------------------------------------------------------------------------*/
function is_numeric(sText)
{
	var ValidChars = "0123456789.";
	var isNumeric=true;
	var Char;
	
	
	for (i = 0; i < sText.length && isNumeric == true; i++) 
	  { 
	  Char = sText.charAt(i); 
	  if (ValidChars.indexOf(Char) == -1) 
		 {
		 isNumeric = false;
		 }
	  }
	return isNumeric;
}
function stringToNumber(st){
	if(st)
	{
		return (typeof(st)=='number' || (typeof(st.match)!='undefined' && !st.match(/[^0-9.,-]/)))?parseFloat(st.replace(/\,/g,'')):st;
	}
	else
	{
		return st;
	}
}
function start_clock()
{
	var thetime=new Date();
	var nhours=thetime.getHours();
	var nmins=thetime.getMinutes();
	var nsecn=thetime.getSeconds();
	var nday=thetime.getDay();
	var nmonth=thetime.getMonth();
	var ntoday=thetime.getDate();
	var nyear=thetime.getYear();
	var AorP=" ";
	if (nhours>=12)
		AorP="P.M.";
	else
		AorP="A.M.";
	if (nhours>=13)
		nhours-=12;
	if (nhours==0)
	   nhours=12;
	if (nsecn<10)
	 nsecn="0"+nsecn;
	if (nmins<10)
	 nmins="0"+nmins;
	$('clockspot').innerHTML=nhours+": "+nmins+": "+nsecn+" "+AorP;
	setTimeout('start_clock()',1000);
}
function switch_display_layer_at_mouse_position(layer_name, offsetX, offsetY)
{
	var object = document.getElementById(layer_name);
	if(object.style.display == 'none')
	{
		display_layer_at_mouse_position(layer_name, offsetX, offsetY);
	}
	else
	{
		object.style.display = 'none';
	}
}
function display_layer_at_mouse_position(layer_name, offsetX, offsetY)
{
	var object = document.getElementById(layer_name);
	if(event.x<document.body.clientWidth-object.style.width.replace('px',''))
	{
		object.style.left = event.x+document.body.scrollLeft+offsetX;
	}
	else
	{
		object.style.left = event.x+document.body.scrollLeft-object.clientWidth+offsetX;
	}
	
	if(event.y<document.body.clientHeight-object.style.height.replace('px',''))
	{
		object.style.top = event.y+document.body.scrollTop+offsetY;
	}
	else
	{
		object.style.top = event.y+document.body.scrollTop-object.clientHeight+offsetY;
	}
	object.style.display = '';
}
/*-------------------- edit by ngocnv 19/05/09 ------------------------*/
function full_screen()
{
	is_full_screen = true;
	if($('_footer_region'))
	{
		$('_header_region').style.display='none';
		$('_footer_region').style.display='none';
		$('_center_region').className='center_region_full_screen';
	}
}
/*-----------------------Print-----------------------------------------------*/
function printWebPart(tagid){
    if (tagid && document.getElementById(tagid)) {
		//build html for print page
		if(jQuery("#"+tagid).attr('type')=='land')
		{
			var content = '<div style="page:land;">';
			content += jQuery("#"+tagid).html();
			content += '</div>';
		}else
		{
        	var content = jQuery("#"+tagid).html();
		}
		var html = "<HTML>\n<HEAD>\n"+
            jQuery("head").html()+
            "\n</HEAD>\n<BODY>\n"+
            content+
            "\n</BODY>\n</HTML>";
        //open new window
        html = html.replace(/<TITLE>((.|[\r\n])*?)<\\?\/TITLE>/ig, "");
		html = html.replace(/<script[^>]*>((.|[\r\n])*?)<\\?\/script>/ig, "");
		var printWP = window.open("","printWebPart");
        printWP.document.open();
        //insert content
        printWP.document.write(html);
        printWP.document.close();
        //open print dialog
        printWP.print();
    }
}
function convertDateToJSDate(date){
	if(date.match(/[0-9]+\/[0-9]+\/[0-9]+/)){
		arr = date.split('/');
		return arr[1]+'-'+arr[0]+'-'+arr[2];
	}else{
		return false;
	}
}
function convertJSDateToDate(JSDate){
	if(JSDate.match(/[0-9]+\-[0-9]+\-[0-9]+/)){
		arr = JSDate.split('/');
		return arr[1]+'/'+arr[0]+'/'+arr[2];
	}else{
		return false;
	}
}
function calculateDays(dateFrom,dateTo){
	var begDate = new Date(convertDateToJSDate(dateFrom));
	var endDate = new Date(convertDateToJSDate(dateTo));
	var difDate = endDate.getTime() - begDate.getTime();
	return difDate/(24*60*60*1000);
}
function select_date_time_range(url, start, end)
{
	if(start>end)
	{
		var temp = start;
		start = end;
		end = temp;
	}
	location = url+start+'-'+end;
}
function openWindowUrl(url,data)
{
	var id = 'new_window';
	var title = '';
	var top = 88;
	var left = 212;
	var width = 600;
	var height = 400
	if(data[0]){
		id = data[0];	
	}
	if(data[1]){
		title = data[1];	
	}
	if(data[2]){
		top = data[2];	
	}
	if(data[3]){
		left = data[3];	
	}
	if(data[4]){
		width = data[4];	
	}
	if(data[5]){
		height = data[5];	
	}	
	jQuery.newWindow({
		id:id,
		title:title,
		content:"",
		posx:left,
		posy:top,
		width:width,
		height:height
	});
	jQuery.updateWindowContentWithUrl(id,url);
}
function str_pad(input, pad_length, pad_string, pad_type) {
    // Returns input string padded on the left or right to specified length with pad_string  
    // 
    // version: 1109.2015
    // discuss at: http://phpjs.org/functions/str_pad
    // +   original by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
    // + namespaced by: Michael White (http://getsprink.com)
    // +      input by: Marco van Oort
    // +   bugfixed by: Brett Zamir (http://brett-zamir.me)
    // *     example 1: str_pad('Kevin van Zonneveld', 30, '-=', 'STR_PAD_LEFT');
    // *     returns 1: '-=-=-=-=-=-Kevin van Zonneveld'
    // *     example 2: str_pad('Kevin van Zonneveld', 30, '-', 'STR_PAD_BOTH');
    // *     returns 2: '------Kevin van Zonneveld-----'
    var half = '',
        pad_to_go;
 
    var str_pad_repeater = function (s, len) {
        var collect = '',
            i;
 
        while (collect.length < len) {
            collect += s;
        }
        collect = collect.substr(0, len);
 
        return collect;
    };
 
    input += '';
    pad_string = pad_string !== undefined ? pad_string : ' ';
 
    if (pad_type != 'STR_PAD_LEFT' && pad_type != 'STR_PAD_RIGHT' && pad_type != 'STR_PAD_BOTH') {
        pad_type = 'STR_PAD_RIGHT';
    }
    if ((pad_to_go = pad_length - input.length) > 0) {
        if (pad_type == 'STR_PAD_LEFT') {
            input = str_pad_repeater(pad_string, pad_to_go) + input;
        } else if (pad_type == 'STR_PAD_RIGHT') {
            input = input + str_pad_repeater(pad_string, pad_to_go);
        } else if (pad_type == 'STR_PAD_BOTH') {
            half = str_pad_repeater(pad_string, Math.ceil(pad_to_go / 2));
            input = half + input + half;
            input = input.substr(0, pad_length);
        }
    }
 
    return input;
}