//= require jquery
//= require jquery_ujs
//= require home/common
//= require home/jquery.dimensions.min
//= require home/jquery.menu
//= require home/jquery.min.1.4.2
//= require home/jquery.ui.core

$(function() {
  $("#products_search").submit(function() {
    $.get(this.action, $(this).serialize(), null, "script");
    return false;
  });
});