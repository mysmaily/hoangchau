class OrderItem < ActiveRecord::Base
  attr_accessible :order_id, :product_id, :quantity, :total_price, :unit_price
  belongs_to :product
  belongs_to :order

  validates :quantity, presence: true, numericality: {greater_than: 0 }

  before_save :finalize

  def unit_price
    if persisted?
      self[:unit_price]
    else
      product.price
    end
  end

  def total_price
    unit_price * quantity
  end

private
  def finalize
    self[:unit_price] = unit_price
    self[:total_price] = quantity * self[:unit_price]
  end
end
