class Order < ActiveRecord::Base
  attr_accessible :customer_name, :customer_number, :sale_off, :tax, :total_price, :status, :user_id, 
                  :final_total, :noted, :giovao, :giora, :created_at
  belongs_to :quanly
  has_many :order_items
  before_save :update_total_price

  def total_price
    order_items.collect { |oi| oi.valid? ? (oi.quantity * oi.unit_price) : 0 }.sum 
  end

  def after_sale_off
    total_price - (sale_off * total_price)/100
  end

  def final_total
    (after_sale_off + (after_sale_off*tax)/100).round(-3)
  end
private

  def update_total_price
    self[:total_price] = total_price
    self[:final_total] = final_total
  end

end
