class Product < ActiveRecord::Base
  attr_accessible :name, :price, :unit, :category_id, :stock_sold
  has_many :order_items
  belongs_to :category
end
