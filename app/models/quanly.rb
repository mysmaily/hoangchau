#encoding: utf-8
class Quanly < ActiveRecord::Base
  self.table_name = "quanly"
  attr_accessible :name, :status, :karaoke, :vip
  has_friendly_id :name, :use_slug => true
  has_many :orders
end
