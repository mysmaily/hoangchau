class ProductsController < ApplicationController
  def index
     if params[:product_search].present?
      session[:product_search] = params[:product_search]
      @products = Product.where("products.name LIKE ?", "%#{session[:product_search]}%")
    else
      @products = Product.all
    end
  end
  
  def new
    @product = Product.new
  end

  def edit
    @product = Product.find(params[:id])
  end

  def create
    @product = Product.create(params[:product])
    redirect_to products_path
  end

  def update
    product = Product.find(params[:id])
    product.update_attributes(params[:product])
    redirect_to products_path
  end

  def destroy
    @product = Product.find(params[:id])
    @product.destroy
    redirect_to products_path
  end

end