class ReportsController < ApplicationController
  
  def index
  end
  def report_bill
    @order = Order.find_by_id(params[:id])
    @bills = @order.order_items
  end

  def reports_detail
    @order = Order.find(params[:order_id])
    @bills = @order.order_items
  end

  def report_revenue
    if params[:start_date].present?
      session[:start_date] = params[:start_date]
      session[:to_date] = params[:to_date]
      start_date = Date.strptime(params[:start_date], "%d-%m-%Y").strftime("%Y-%m-%d")
      to_date = Date.strptime(params[:to_date], "%d-%m-%Y").strftime("%Y-%m-%d")
      if params[:bill].present?
        @orders = Order.where(:status => "complete").where("orders.created_at >= ? AND orders.created_at <= ?", start_date.to_date, to_date.to_date + 1.day)
        if @orders.empty?
          @nodata = true
        end
      elsif params[:products].present?
        @products = Product.joins(:order_items).group("order_items.product_id").
                    where("order_items.created_at >= ? AND order_items.created_at <= ?", start_date.to_date, to_date.to_date + 1.day).
                    where("stock_sold > ?", 0)
        if @products.empty?
          @nodata = true
        end
      end
    end
  end

  def report_product
    @products = Product.where("stock_sold > ?", 0)
  end

end
