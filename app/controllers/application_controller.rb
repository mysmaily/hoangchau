class ApplicationController < ActionController::Base
  protect_from_forgery
  before_filter :authenticate_user!, :get_category
  helper_method :current_table, :current_order

  def get_category
  	@categories = Category.all.map{ |p| [p.name, p.id]}
  end
  def current_table
    Quanly.find(session[:table_id])
  end
  def current_order
    Order.find(session[:order_id])
  end
end
