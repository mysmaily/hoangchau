class OrderItemsController < ApplicationController
  def create
    @order = current_order
    if params[:add].present?
      update = []
      @order.order_items.each do |t|
        if t.product_id == params[:order_item][:product_id].to_i && t.order_id == @order.id
          order_item = OrderItem.where(order_id: @order.id, product_id: params[:order_item][:product_id]).first
          update << order_item.update_attributes(:quantity => t.quantity + params[:order_item][:quantity].to_f)
        end
      end
      if update.empty?
        @order_item = @order.order_items.create(params[:order_item])
      end
    elsif params[:cut_down].present?
      order_item = OrderItem.where(order_id: @order.id, product_id: params[:order_item][:product_id]).first
      order_item.update_attributes(:quantity => order_item.quantity - params[:order_item][:quantity].to_f)
    end
    session[:tab] = params[:tab]
    redirect_to quanly_path(current_table.id)
  end

  def update
    @order = current_order
    @order_item = @order.order_items.find(params[:id])
    @order_item.update_attributes(order_item_params)
    @order_items = @order.order_items
  end

  def destroy
    @order = current_order
    @order_item = @order.order_items.find(params[:id])
    @order_item.destroy
    redirect_to :back
  end
end