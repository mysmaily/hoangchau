class HomeController < ApplicationController
  def index
  	session.delete(:start_date)
  	session.delete(:to_date)
    @tables = Quanly.where(:karaoke => false)
    @rooms = Quanly.where(:karaoke => true)
  end
end