class QuanlyController < ApplicationController

  def show
    @table = Quanly.find(params[:id])
    if params[:product_search].present?
      @products = Product.where("name LIKE ?", "%#{params[:product_search]}%")
      session[:tab] = "tc"
    else
      @products = Product.all
    end
    @service_products = Product.where(category_id: 3)
    @da_products = Product.where(category_id: 1)
    @du_products = Product.where(category_id: 2)
    session[:table_id] = @table.id
    if current_table.orders.present? && current_table.orders.last.status == 'wait'
      @order = Order.where(quanly_id: current_table.id, status: 'wait').first
      session[:order_id] = @order.id
      @order_item = current_order.order_items.new
      @order_items = current_order.order_items
    else
      @order = current_table.orders.new
    end
    @order_new = current_table.orders.new
  end

  def create_orders
    @table = current_table
    @table.orders.new(params[:order])
    if @table.orders.save!
      @table.update_attributes(:status => 'open')
    end
  end

  def update_table_status
    @table = current_table
    order = current_order
    @table.update_attributes(:status => 'closed')
    order.update_attributes(:status => 'complete')
    if params[:save].present?
      order = Order.where(:id => current_order.id).first
      order.update_attributes(params[:order])
      redirect_to :back
    elsif params[:checkout].present?
      @table.update_attributes(:status => 'closed')
      current_order.update_attributes(:status => 'complete')
      redirect_to root_path
    end
  end
end