class OrdersController < ApplicationController
  respond_to :html, :xml, :json
  http_basic_authenticate_with name: "phuc@gmail.com", password: "tuyenthanh", only: :delete_order
  def new
    @order = current_table.orders.new
    respond_with(@order)
  end
  def create
    @table = current_table
    if params[:checkin].present?
      @table.update_attributes(:status => 'open')
      params[:order][:created_at] = Time.now.utc + 7.hours
      @table.orders.create(params[:order])
      if @table.karaoke == true
        order = Order.where(id: @table.orders.last.id).first
        if order.quanly.vip == false
          order.order_items.create(:product_id => 36, :quantity => 1)
        else
          order.order_items.create(:product_id => 37, :quantity => 1)
        end
      end
      redirect_to :back
    elsif params[:save].present?
      order = current_order
      if @table.karaoke == true
        start_time = Time.now.to_date.to_s + " #{params[:start_hour].chop}:#{params[:start_min]}"
        end_time = Time.now.to_date.to_s + " #{params[:end_hour].chop}:#{params[:end_min]}"
        params[:order][:giovao] = "#{params[:start_hour].chop}:#{params[:start_min]}"
        params[:order][:giora] = "#{params[:end_hour].chop}:#{params[:end_min]}"
        time = (end_time.to_time - start_time.to_time).divmod(60)[0]
        if time < 0
          time = time + 1440
        end
        if order.quanly.vip == true
          if order.order_items.where(:product_id => 37).count != 0
            order.order_items.where(:product_id => 37).first.update_attributes(:quantity => time/60.round(2) )
          else
            order.order_items.create(product_id: 37, quantity: 1)
          end
        else
          if order.order_items.where(:product_id => 36).count != 0
            order.order_items.where(:product_id => 36).first.update_attributes(:quantity => time/60.round(2))
          else
            order.order_items.create(product_id: 36, quantity: 1)
          end
        end
      end
      order.update_attributes(params[:order])
      redirect_to :back
    elsif params[:checkout].present?
      count = current_order.order_items("product_id")
      count.each do |t|
        product = Product.where("id = ?", t.product_id).first
        product.update_attributes(:stock_sold => product.stock_sold + t.quantity)
      end
        
      @table.update_attributes(:status => 'closed')
      current_order.update_attributes(:status => 'complete')
      redirect_to root_path
    end
  end

  def delete_order
    order = Order.find(params[:format])
    order_items = order.order_items
    order_items.each do |t|
      product = Product.find_by_id(t.product_id)
      product.update_attributes(:stock_sold => product.stock_sold - t.quantity)
    end
    order_items.destroy_all
    order.destroy
    redirect_to :back
  end
end